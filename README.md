

# BUILD

```bash
cd node-printer-0.2.2
debuild -S
```

# UPLOAD

```bash
dput ppa:yourlaunchpadid/yourppa *.changes
```
